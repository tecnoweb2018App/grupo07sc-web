<?php
include_once 'core/db.php';

class vendedor
{
    private $pdo;
    public $id;
    public $nombre;
    public $imagen;
    public $celular;
    public $direccion;

    public function __CONSTRUCT()
    {
        try
        {
            $this->pdo = Database::StartUp();
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Listar()
    {
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT id, nombre, celular, direccion FROM usuarios where tipo=1");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Obtener($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("SELECT * FROM usuarios WHERE id = ?");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM usuarios WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


    public function Actualizar($data)
    {
        try
        {
            $sql = "UPDATE usuarios SET 
						nombre =?,
						celular =?,
						direccion =?
					    WHERE id	= ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->celular,
                        $data->direccion,
                        $data->id
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Registrar(Vendedor $data)
    {
        try
        {
            $sql = "INSERT INTO usuarios (nombre,celular,direccion, tipo) VALUES (?, ?, ?, ?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->celular,
                        $data->direccion,
                        $data->tipo
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
}