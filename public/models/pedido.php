<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 18-11-18
 * Time: 10:03 PM
 */

require_once 'core/db.php';

class pedido{
    private $pdo;
    public $id;
    public $cantidad;
    public $total;
    public $latitud;
    public $longitud;
    public $tipopedido;
    public $estadopedido;
    public $fecha;
    public $hora;
    public $idcliente;
    public $idvendedor;
    public $descripcion;

    public function __CONSTRUCT()
    {
        try {
            $this->pdo = Database::StartUp();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Listar()
    {
        try {
            $result = array();
            $stm = $this->pdo->prepare("SELECT id, cantidad, total, latitud, longitud, tipopedido, estadopedido, fecha, hora, idcliente, idvendedor, descripcion FROM pedidos");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Obtener($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("SELECT * FROM pedidos WHERE id = ?");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM pedidos WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Actualizar($data)
    {
        try
        {
            $sql = "UPDATE pedidos SET 
						cantidad =?,
						total =?,
						latitud =?,
						longitud ?,
						tipopedido =?,
						estadopedido =?,
						fecha =?,
						hora = ?,
						idcliente = ?,
						idvendedor =?,
						descripcion =?
				    	WHERE id	= ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->cantidad,
                        $data->total,
                        $data->latitud,
                        $data->longitud,
                        $data->tipopedido,
                        $data->estadopedido,
                        $data->fecha,
                        $data->hora,
                        $data->idcliente,
                        $data->idvendedor,
                        $data->descripcion,
                        $data->id
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


}