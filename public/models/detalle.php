<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 18-11-18
 * Time: 10:03 PM
 */

require_once 'core/db.php';

class detalle{
    private $pdo;
    public $idpedido;
    public $idproducto;
    public $cantidad;
    public $subtotal;
    public $precio;

    public function __CONSTRUCT()
    {
        try {
            $this->pdo = Database::StartUp();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Listar()
    {
        try {
            $result = array();
            $stm = $this->pdo->prepare("SELECT idpedido, idproducto, cantidad, subtotal, precio FROM detalles");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

        public function Obtener($id)
        {
            try
            {
                $stm = $this->pdo
                    ->prepare("SELECT * FROM detalles WHERE idpedido = ?");


                $stm->execute(array($id));
                return $stm->fetch(PDO::FETCH_OBJ);
            } catch (Exception $e)
            {
                die($e->getMessage());
            }
        }

        public function Eliminar($id)
        {
            try
            {
                $stm = $this->pdo
                    ->prepare("DELETE FROM detalles WHERE idpedido = ?");

                $stm->execute(array($id));
            } catch (Exception $e)
            {
                die($e->getMessage());
            }
        }

        public function Registrar(Detalles $data)
        {
            try
            {
                $sql = "INSERT INTO detalles (idpedido, idproducto, cantidad, subtotal, precio) VALUES (?, ?, ?, ?, ?)";

                $this->pdo->prepare($sql)
                    ->execute(
                        array(
                            $data->idpedido,
                            $data->idproducto,
                            $data->cantidad,
                            $data->subtotal,
                            $data->precio
                        )
                    );
            } catch (Exception $e)
            {
                die($e->getMessage());
            }
        }

}