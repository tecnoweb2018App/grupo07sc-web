<?php
include_once 'core/db.php';

class cliente
{
    private $pdo;

    public $id;
    public $nombre;
    public $imagen;
    public $correo;
    public $contrasena;
    public $celular;
    public $direccion;
    public $latitud;
    public $longitud;

    public function __CONSTRUCT()
    {
        try {
            $this->pdo = Database::StartUp();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Listar()
    {
        try {
            $result = array();
            $stm = $this->pdo->prepare("SELECT id, nombre,imagen,correo,contrasena,celular,direccion,latitud,longitud FROM usuarios where tipo=2");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Obtener($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("SELECT * FROM usuarios WHERE id = ?");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM usuarios WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Actualizar($data)
    {
        try
        {
            $sql = "UPDATE usuarios SET 
						nombre =?,
						imagen =?,
						correo =?,
						contrasena =?,
						celular =?,
						direccion =?,
						latitud =?,
						longitud =?
				    	WHERE id	= ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->imagen,
                        $data->correo,
                        $data->contrasena,
                        $data->celular,
                        $data->direccion,
                        $data->latitud,
                        $data->longitud,
                        $data->id
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
    public function Registrar(Cliente $data)
    {
        try
        {
            $sql = "INSERT INTO usuarios (nombre,imagen,correo,contrasena,celular,direccion,latitud,longitud) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->imagen,
                        $data->correo,
                        $data->contrasena,
                        $data->celular,
                        $data->direccion,
                        $data->latitud,
                        $data->longitud
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
}