<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 18-11-18
 * Time: 09:50 PM
 */
include_once 'core/db.php';

class producto{
        private $pdo;
        public $id;
        public $nombre;
        public $descripcion;
        public $precio;
        public $tamano;
        public $imagen;
        public $idtipo;

    public function __CONSTRUCT()
    {
        try {
            $this->pdo = Database::StartUp();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Listar()
    {
        try {
            $result = array();
            $stm = $this->pdo->prepare("select productos.id as id, productos.nombre as nombre, productos.descripcion as descripcion, productos.precio as precio, tipo.nombre as idtipo
from productos, tipo where productos.idtipo = tipo.id");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Obtener($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("SELECT * FROM productos WHERE id = ?");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM productos WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
    public function Actualizar($data)
    {
        try
        {
            $sql = "UPDATE productos SET 
						nombre =?,
						descripcion =?,
						precio =?,
						tamano =?,
						imagen =?,
						idtipo =?
				    	WHERE id	= ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->descripcion,
                        $data->precio,
                        $data->tamano,
                        $data->imagen,
                        $data->idtipo,
                        $data->id
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
    public function Registrar(Producto $data)
    {
        try
        {
            $sql = "INSERT INTO productos (nombre, descripcion, precio, tamano, imagen, idtipo) VALUES (?, ?, ?, ?, ?, ?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->descripcion,
                        $data->precio,
                        $data->tamano,
                        $data->imagen,
                        $data->idtipo
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


}