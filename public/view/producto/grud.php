<form action="?c=producto&a=Guardar" method="post" enctype="multipart/form-data">
    <input type="text" class="form-control" hidden name="id" value="<?php echo $producto->id; ?>">
    <div class="form-element-list mg-t-30">

        <div class="container">
            <div class="cmp-tb-hd">
                <h2>Gestion de los productos</h2>
                <p>Puede actulizar los proudctos. </p>
            </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="form-group ic-cmp-int float-lb floating-lb">
                    <div class="form-ic-cmp">
                        <i class="notika-icon notika-support"></i>
                    </div>
                    <div class="nk-int-st">
                        <input type="text" class="form-control" name="nombre" value="<?php echo $producto->nombre; ?>">
                        <label class="nk-label">Nombre</label>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="form-group ic-cmp-int float-lb floating-lb">
                    <div class="form-ic-cmp">
                        <i class="notika-icon notika-mail"></i>
                    </div>
                    <div class="nk-int-st">
                        <input type="text" class="form-control" name="descripcion" value="<?php echo $producto->descripcion; ?>">
                        <label class="nk-label">Descripcion</label>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="form-group ic-cmp-int float-lb floating-lb">
                    <div class="form-ic-cmp">
                        <i class="notika-icon notika-phone"></i>
                    </div>
                    <div class="nk-int-st">
                        <input type="text" class="form-control" name="precio" value="<?php echo $producto->precio; ?>">
                        <label class="nk-label">Precio</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="form-group ic-cmp-int float-lb form-elet-mg">
                    <div class="form-ic-cmp">
                        <i class="notika-icon notika-wifi"></i>
                    </div>
                    <div class="nk-int-st">
                        <input type="text" class="form-control" name="tamano" value="<?php echo $producto->tamano; ?>">
                        <label class="nk-label">Tamano</label>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="form-group ic-cmp-int float-lb form-elet-mg">
                    <div class="form-ic-cmp">
                        <i class="notika-icon notika-house"></i>
                    </div>
                    <div class="nk-int-st">
                        <input type="text" class="form-control" name="imagen" value="<?php echo $producto->imagen; ?>">
                        <label class="nk-label">images</label>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="chosen-select-act fm-cmp-mg">
                        <label>Tipo de producto</label>
                        <select name="idtipo" type="text" class="form-control py-5 py-md-6" aria-required="true" value="<?php echo $producto->idtipo; ?>">
                            <?php foreach($this->modelstipo->Listar() as $h): ?>
                                <option value="<?php echo $h->id; ?>"><?php echo $h->nombre; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-danger notika-btn-danger waves-effect pad-r20">Guardar</button>

        </div>
        </div>
    </div>

</form>