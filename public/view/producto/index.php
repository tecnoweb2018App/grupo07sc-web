
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="normal-table-list">
                <div class="basic-tb-hd">
                    <h2>Lista de Producto</h2>
                </div>
                <div class="bsc-tbl">

                    <table id="example" class="display nowrap" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Descripcion</th>
                            <th>Precio</th>
                            <th>Tipo de producto</th>

                            <th>Accion</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($this->model->Listar() as $producto): ?>
                            <tr>
                                <td><?php echo $producto->id; ?></td>
                                <td><?php echo $producto->nombre; ?></td>
                                <td><?php echo $producto->descripcion; ?></td>
                                <td><?php echo $producto->precio; ?></td>
                                <td><?php echo $producto->idtipo; ?></td>

                                <td>
                                    <a class="btn btn-info" href="?c=producto&a=Crud&id=<?php echo $producto->id; ?>">Actulizar</a>
                                    <a class="btn btn-danger" href="?c=producto&a=Eliminar&id=<?php echo $producto->id; ?>">Eliminar</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <script>
                        $.noConflict();
                        jQuery( document ).ready(function( $ ) {
                            $('#example').DataTable({
                                "order": [[ 3, "desc" ]]
                            });
                        });
                        // Code that uses other library's $ can follow here.
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>