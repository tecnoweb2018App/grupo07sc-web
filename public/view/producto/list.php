<div class="container">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="animation-single-int mg-t-30">
                <div class="animation-ctn-hd">
                    <h2>Sliding Entrances</h2>
                    <p>Click on the buttons below to start the animation action in image.</p>
                </div>
                <div class="animation-img mg-b-15">
                    <img class="animate-seven" src="img/widgets/2.png" alt="">
                </div>
                <div class="animation-action">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="animation-btn">
                                <button class="btn ant-nk-st slideInUp-ac waves-effect">slideInUp</button>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="animation-btn sm-res-mg-t-10 tb-res-mg-t-10 dk-res-mg-t-10">
                                <button class="btn ant-nk-st slideInDown-ac waves-effect">slideInDown</button>
                            </div>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <div class="col-lg-6">
                            <div class="animation-btn">
                                <button class="btn ant-nk-st slideInLeft-ac waves-effect">slideInLeft</button>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="animation-btn sm-res-mg-t-10 tb-res-mg-t-10 dk-res-mg-t-10">
                                <button class="btn ant-nk-st slideInRight-ac waves-effect">slideInRight</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mg-t-30">
            <div class="animation-single-int">
                <div class="animation-ctn-hd">
                    <h2>Rotating Entrances</h2>
                    <p>Click on the buttons below to start the animation action in image.</p>
                </div>
                <div class="animation-img mg-b-15">
                    <img class="animate-eight" src="img/widgets/4.png" alt="">
                </div>
                <div class="animation-action">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="animation-btn">
                                <button class="btn ant-nk-st rotateIn-ac waves-effect">rotateIn</button>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="animation-btn sm-res-mg-t-10 tb-res-mg-t-10 dk-res-mg-t-10">
                                <button class="btn ant-nk-st rotateInDownLeft-ac waves-effect">rotateInDownLeft</button>
                            </div>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <div class="col-lg-6">
                            <div class="animation-btn">
                                <button class="btn ant-nk-st rotateInDownRight-ac waves-effect">rotateInDownRight</button>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="animation-btn sm-res-mg-t-10 tb-res-mg-t-10 dk-res-mg-t-10">
                                <button class="btn ant-nk-st rotateInUpLeft-ac waves-effect">rotateInUpLeft</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mg-t-30">
            <div class="animation-single-int">
                <div class="animation-ctn-hd">
                    <h2>Rotating Exits</h2>
                    <p>Click on the buttons below to start the animation action in image.</p>
                </div>
                <div class="animation-img mg-b-15">
                    <img class="animate-nine" src="img/widgets/6.png" alt="">
                </div>
                <div class="animation-action">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="animation-btn">
                                <button class="btn ant-nk-st rotateOut-ac waves-effect">rotateOut</button>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="animation-btn sm-res-mg-t-10 tb-res-mg-t-10 dk-res-mg-t-10">
                                <button class="btn ant-nk-st rotateOutDownLeft-ac waves-effect">rotateOutDownLeft</button>
                            </div>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <div class="col-lg-6">
                            <div class="animation-btn">
                                <button class="btn ant-nk-st rotateOutDownRight-ac waves-effect">rotateOutDownRight</button>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="animation-btn sm-res-mg-t-10 tb-res-mg-t-10 dk-res-mg-t-10">
                                <button class="btn ant-nk-st rotateOutUpLeft-ac waves-effect">rotateOutUpLeft</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>