<div class="container">
    <div class="row">
        <form action="?c=vendedor&a=Guardar" class="form-example-wrap mg-t-30" method="post" enctype="multipart/form-data">
            <input type="text" class="form-control" hidden name="id" value="<?php echo $vendedor->id; ?>">

            <div class="cmp-tb-hd cmp-int-hd">
                <h2>Nuevo vendedor</h2>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-example-int form-example-st">
                        <div class="form-group">
                            <div class="nk-int-st">
                                <input type="text" name="nombre" class="form-control input-sm" placeholder="Nombre" value="<?php echo $vendedor->nombre; ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-example-int form-example-st">
                        <div class="form-group">
                            <div class="nk-int-st">
                                <input type="text" class="form-control input-sm" name="celular" placeholder="celular" value="<?php echo $vendedor->celular; ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-example-int form-example-st">
                        <div class="form-group">
                            <div class="nk-int-st">
                                <input type="text" class="form-control input-sm" name="direccion" placeholder="direccion" value="<?php echo $vendedor->direccion; ?>">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <hr>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
                    <div class="form-example-int">
                        <button class="btn btn-success notika-btn-success waves-effect">Guardar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>