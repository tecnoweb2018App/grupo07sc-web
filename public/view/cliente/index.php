
<div class="container">
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="normal-table-list">
            <div class="basic-tb-hd">
                <h2>Lista de clientes</h2>
            </div>
            <div class="bsc-tbl">

                <table id="example" class="display nowrap" width="100%">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>Celular</th>
                        <th>direccion</th>
                        <th>Accion</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($this->model->Listar() as $cliente): ?>
                    <tr>
                        <td><?php echo $cliente->id; ?></td>
                        <td><?php echo $cliente->nombre; ?></td>
                        <td><?php echo $cliente->correo; ?></td>
                        <td><?php echo $cliente->celular; ?></td>
                        <td><?php echo $cliente->direccion; ?></td>
                        <td>
                            <a class="btn btn-info" href="?c=cliente&a=Crud&id=<?php echo $cliente->id; ?>">Actulizar</a>
                            <a class="btn btn-danger" href="?c=cliente&a=Eliminar&id=<?php echo $cliente->id; ?>">Eliminar</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <script>
                    $.noConflict();
                    jQuery( document ).ready(function( $ ) {
                        $('#example').DataTable({
                            "order": [[ 3, "desc" ]]
                        });
                    });
                    // Code that uses other library's $ can follow here.
                </script>
            </div>
        </div>
    </div>
</div>
</div>