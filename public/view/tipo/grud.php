<form action="?c=tipo&a=Guardar" method="post" enctype="multipart/form-data">

<div class="container">
    <form action="">
    <div class="form-element-list mg-t-30">
        <div class="cmp-tb-hd">
            <h2>Gestion de tipo</h2>
            <p>Gestion de tipo de productos para la ventas</p>
        </div>
        <input type="text" class="form-control" hidden name="id" value="<?php echo $tipo->id; ?>">

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="nk-int-mk">
                    <h2>Nombre de tipo</h2>
                </div>
                <div class="form-group ic-cmp-int">
                    <div class="form-ic-cmp">
                        <i class="notika-icon notika-support"></i>
                    </div>
                    <div class="nk-int-st">
                        <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="<?php echo $tipo->nombre; ?>">
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="nk-int-mk">
                    <h2>Descripcion</h2>
                </div>
                <div class="form-group ic-cmp-int">
                    <div class="form-ic-cmp">
                        <i class="notika-icon notika-mail"></i>
                    </div>
                    <div class="nk-int-st">
                        <input type="text" class="form-control" placeholder="descripcion" name="descripcion" value="<?php echo $tipo->descripcion; ?>">
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="nk-int-mk">
                    <h2>Imagen</h2>
                </div>
                <div class="form-group ic-cmp-int">
                    <div class="form-ic-cmp">
                        <i class="notika-icon notika-tax"></i>
                    </div>
                    <div class="nk-int-st">
                        <input type="text" class="form-control" name="imagen" placeholder="imagen" <?php echo $tipo->imagen; ?>>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary notika-btn-primary btn-lg waves-effect">Guardar</button>
        </div>
    </div>
    </form>
</div>