
<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Dashboard One | Notika - Notika Admin Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="assert/img/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="assert/css/bootstrap.min.css">
    <link rel="stylesheet" href="assert/css/font-awesome.min.css">
    <link rel="stylesheet" href="assert/css/owl.carousel.css">
    <link rel="stylesheet" href="assert/css/owl.theme.css">
    <link rel="stylesheet" href="assert/css/owl.transitions.css">
    <link rel="stylesheet" href="assert/css/meanmenu/meanmenu.min.css">
    <link rel="stylesheet" href="assert/css/animate.css">
    <link rel="stylesheet" href="assert/css/normalize.css">
    <link rel="stylesheet" href="assert/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="assert/css/jvectormap/jquery-jvectormap-2.0.3.css">
    <link rel="stylesheet" href="assert/css/notika-custom-icon.css">
    <link rel="stylesheet" href="assert/css/wave/waves.min.css">
    <link rel="stylesheet" href="assert/css/main.css">
    <link rel="stylesheet" href="assert/style.css">
    <link rel="stylesheet" href="assert/css/responsive.css">
    <script src="assert/js/vendor/modernizr-2.8.3.min.js"></script>
    <script src="assert/jquery-3.3.1.js"></script>

    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>

    <link href="https://nightly.datatables.net/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <script src="https://nightly.datatables.net/js/jquery.dataTables.js"></script>

</head>
<body>

<?php
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);
?>


<!-- Start Header Top Area -->
<?php require_once 'view/template/header.php'?>
<!-- End Header Top Area -->
<!-- Mobile Menu start -->
<?php require_once 'view/template/Mobile.php'?>
<!-- Mobile Menu end -->
<!-- Main Menu area start-->
<?php require_once 'view/template/Main.php'?>
<!-- Main Menu area End-->

<?php
$controller = 'dasboard';

require_once 'core/shared.php';
?>


<script src="assert/js/vendor/jquery-1.12.4.min.js"></script>
<script src="assert/js/bootstrap.min.js"></script>
<script src="assert/js/wow.min.js"></script>
<script src="assert/js/jquery-price-slider.js"></script>
<script src="assert/js/owl.carousel.min.js"></script>
<script src="assert/js/jquery.scrollUp.min.js"></script>
<script src="assert/js/meanmenu/jquery.meanmenu.js"></script>
<script src="assert/js/counterup/jquery.counterup.min.js"></script>
<script src="assert/js/counterup/waypoints.min.js"></script>
<script src="assert/js/counterup/counterup-active.js"></script>
<script src="assert/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="assert/js/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script src="assert/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="assert/js/jvectormap/jvectormap-active.js"></script>
<script src="assert/js/sparkline/jquery.sparkline.min.js"></script>
<script src="assert/js/sparkline/sparkline-active.js"></script>
<script src="assert/js/flot/jquery.flot.js"></script>
<script src="assert/js/flot/jquery.flot.resize.js"></script>
<script src="assert/js/flot/curvedLines.js"></script>
<script src="assert/js/flot/flot-active.js"></script>
<script src="assert/js/knob/jquery.knob.js"></script>
<script src="assert/js/knob/jquery.appear.js"></script>
<script src="assert/js/knob/knob-active.js"></script>
<script src="assert/js/wave/waves.min.js"></script>
<script src="assert/js/wave/wave-active.js"></script>
<script src="assert/js/todo/jquery.todo.js"></script>
<script src="assert/js/plugins.js"></script>
<script src="assert/js/chat/moment.min.js"></script>
<script src="assert/js/chat/jquery.chat.js"></script>
<script src="assert/js/main.js"></script>
<script src="assert/js/tawk-chat.js"></script>
</body>
</html>