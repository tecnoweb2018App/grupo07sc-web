<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 19-11-18
 * Time: 12:24 AM
 */
require_once 'models/vendedor.php';

class vendedorController{

    public function __CONSTRUCT(){
        $this->model = new vendedor();
    }

    public function Index(){
        require_once 'view/vendedor/index.php';

    }
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        require_once 'view/vendedor/index.php';
    }


    public function Crud(){
        $vendedor = new vendedor();

        if(isset($_REQUEST['id'])){
            $vendedor = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'view/vendedor/grud.php';

    }

    public function Guardar(){
        $vendedor = new vendedor();

        $vendedor->id = $_REQUEST['id'];
        $vendedor->nombre = $_REQUEST['nombre'];
        $vendedor->celular = $_REQUEST['celular'];
        $vendedor->direccion = $_REQUEST['direccion'];
        $vendedor->tipo = 1;

        $vendedor->id > 0
            ? $this->model->Actualizar($vendedor)
            : $this->model->Registrar($vendedor);

        require_once 'view/vendedor/index.php';
    }


}