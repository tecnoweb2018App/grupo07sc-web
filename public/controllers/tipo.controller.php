<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 19-11-18
 * Time: 12:24 AM
 */
require_once 'models/tipo.php';

class TipoController{

    public function __CONSTRUCT(){
        $this->model = new tipo();
    }

    public function Index(){
        require_once 'view/tipo/index.php';

    }

    public function Crud(){
        $tipo = new tipo();

        if(isset($_REQUEST['id'])){
            $tipo = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'view/tipo/grud.php';
    }
    public function Guardar(){
        $tipo = new tipo();

        $tipo->id = $_REQUEST['id'];
        $tipo->nombre = $_REQUEST['nombre'];
        $tipo->descripcion = $_REQUEST['descripcion'];
        $tipo->imagen = $_REQUEST['imagen'];

        $tipo->id > 0
            ? $this->model->Actualizar($tipo)
            : $this->model->Registrar($tipo);

        require_once 'view/tipo/index.php';
    }
}