<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 19-11-18
 * Time: 12:24 AM
 */
require_once 'models/producto.php';
require_once 'models/tipo.php';


class productoController{
    private $models;
    private $modelstipo;

    public function __CONSTRUCT(){
        $this->model = new producto();
        $this->modelstipo = new tipo();

    }

    public function obtenerTipo(){
        $producto = new producto();
        $producto = $this->modelstipo->Listar();
        require_once 'view/producto/index.php';

    }

    public function Index(){
        require_once 'view/producto/index.php';

    }
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        require_once 'view/producto/index.php';
    }

    public function Crud(){
        $producto = new producto();

        if(isset($_REQUEST['id'])){
            $producto = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'view/producto/grud.php';

    }
    public function lista(){
        require_once 'view/producto/list.php';

    }

    public function Guardar(){
        $producto = new producto();

        $producto->id = $_REQUEST['id'];
        $producto->nombre = $_REQUEST['nombre'];
        $producto->descripcion = $_REQUEST['descripcion'];
        $producto->precio = $_REQUEST['precio'];
        $producto->tamano = $_REQUEST['tamano'];
        $producto->imagen = $_REQUEST['imagen'];
        $producto->idtipo = $_REQUEST['idtipo'];

        $producto->id > 0
            ? $this->model->Actualizar($producto)
            : $this->model->Registrar($producto);

        require_once 'view/producto/index.php';
    }

}