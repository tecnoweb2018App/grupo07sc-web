<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 19-11-18
 * Time: 12:24 AM
 */
require_once 'models/cliente.php';

class clienteController{

    public function __CONSTRUCT(){
        $this->model = new cliente();
    }

    public function Index(){
        require_once 'view/cliente/index.php';

    }

    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        require_once 'view/cliente/index.php';
    }


    public function Crud(){
        $cliente = new cliente();

        if(isset($_REQUEST['id'])){
            $cliente = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'view/cliente/grud.php';

    }

    public function Guardar(){
        $cliente = new cliente();

        $cliente->id = $_REQUEST['id'];
        $cliente->nombre = $_REQUEST['nombre'];
        $cliente->imagen = $_REQUEST['imagen'];
        $cliente->correo = $_REQUEST['correo'];
        $cliente->contrasena = $_REQUEST['contrasena'];
        $cliente->celular = $_REQUEST['celular'];
        $cliente->direccion = $_REQUEST['direccion'];
        $cliente->latitud = $_REQUEST['latitud'];
        $cliente->longitud = $_REQUEST['longitud'];

        $cliente->id > 0
            ? $this->model->Actualizar($cliente)
            : $this->model->Registrar($cliente);

        require_once 'view/cliente/index.php';
    }

}