<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 19-11-18
 * Time: 12:24 AM
 */
require_once 'models/pedido.php';

class vendedorController{

    public function __CONSTRUCT(){
        $this->model = new pedido();
    }

    public function Index(){
        require_once 'view/pedido/index.php';

    }
    public function Crud(){
        $pedido = new pedido();

        if(isset($_REQUEST['id'])){
            $pedido = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'view/vendedor/grud.php';

    }

}